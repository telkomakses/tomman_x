"use strict";
/* jshint esversion: 6 */
/* jshint worker: true */

const CACHE_NAME = 'SWv1';
const URL_SHELL = '/app-shell';
const URL_LOGIN = '/login';
const URL_LOGOUT = '/logout';

self.onactivate = function(event) {
  let currentCacheName = CACHE_NAME;
  caches.keys().then(cacheNames => {
    return Promise.all(
      cacheNames.map(cacheName => {
        if (cacheName !== currentCacheName)
          return caches.delete(cacheName);
      })
    );
  });
};

self.onfetch = function(event) {
  let request = event.request;
  let url = new URL(request.url);

  // skip service worker for external resources and certain urls
  if (
    url.host !== location.host
    || (
      url.pathname.indexOf('.') !== -1 // file with extension
      || url.pathname.indexOf('/partial/') === 0
      || url.pathname.indexOf('/api/') !== -1 // json api request
      || url.pathname === URL_LOGIN
    )
  ) {
    return;
  }

  // destroy shell cache on logout
  if (url.pathname === URL_LOGOUT) {
    event.respondWith(
      caches.open(CACHE_NAME)
        .then(cache => cache.delete(URL_SHELL))
        .then(_ => {
          let req = new Request(url, {
            headers: request.headers,
            mode: 'same-origin',
            credentials: request.credentials,
            redirect: 'manual'
          });
          return fetch(req, { credentials: 'include' });
        })
    );
  } else {
    event.respondWith(
      caches.match(request).then(response => {
        if (response) {
          return response;
        }

        let shellRequest = new Request(URL_SHELL, { redirect: 'manual' });

        return caches.match(shellRequest).then(cached => {
          if (cached) {
            return cached;
          }

          return fetch(shellRequest, {credentials: 'include'}).then(response => {
            let resp = response.clone();
            return caches.open(CACHE_NAME).then(c => c.put(shellRequest.url, resp)).then(_ => response);
          })
        });
      })
    );
  }
};
