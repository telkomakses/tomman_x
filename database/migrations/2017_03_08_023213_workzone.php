<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Workzone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE workzone(
              id smallserial PRIMARY KEY,
              nama text NOT NULL CHECK (nama <> ''),
              path ltree
            )
        ");

        DB::statement("CREATE INDEX ON workzone USING GIST(path)");
        DB::statement("CREATE INDEX ON workzone USING btree(path)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE workzone');
    }
}
