<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class McoreSto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE TABLE mcore.sto(
              id          serial PRIMARY KEY,
              workzone_id smallint REFERENCES workzone(id),
              nama        text NOT NULL CHECK (nama <> ''),
              koordinat   geometry(POINT, 4326),
              
              UNIQUE(workzone_id, nama)
            );
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TABLE mcore.sto');
    }
}
