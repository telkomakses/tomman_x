@extends('bot')

@section('style')
  <style>
    .panel-heading {
      font-size: 20px;
    }

    .flex-container {
      display: flex;
      align-items: stretch;
      margin: -5px;
    }
    .flex-container > div {
      margin: 5px;
      flex-grow: 1;
      flex-basis: 0;
    }
    .card {
      background-color: #fff;
      box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.2), 0 1px 5px 0 rgba(0,0,0,.12);
      border: 1px solid rgba(0,0,0,.12);
      padding: 10px;
    }
    .card td {
      padding: 2px 4px;
    }
    .card th {
      padding-top: 10px;
    }
    .card-footer {
      color: #fff;
      margin: 10px -10px -10px;
      padding: 10px;
    }
    .card-footer .fa {
      margin-right: 4.5px;
    }

    .entry > div:first-child {
      color: black;
    }
    .text-big {
      font-size: 24px;
      font-weight: 500;
    }
    .entry {
      margin-bottom: 17px;
    }

    .billing-latest {
      display: flex;
      flex-direction: column;
    }
    .billing-latest > div {
      flex: 1;
    }
    .billing-latest div:nth-child(2) {
      flex: 0;
    }

    .history-previous {
      margin-top: 20px;
    }
    .history-previous .panel-body, .history-previous .panel-heading {
      padding: 10px;
    }
  </style>
@endsection

@section('body')
  <div class="flex-container">
    <div class="card">
      <h1>{{ $data->nama }}</h1>

      <div class="entry">
        <div>NOMOR TELEPON</div>
        <div class="text-big">{{ $data->phone }}</div>
      </div>
      <div class="entry">
        <div>NOMOR INTERNET</div>
        <div class="text-big">{{ $data->internet }}</div>
      </div>
      <div class="entry">
        <div>PRODUK</div>
        <div class="text-big">
          <?php $token = preg_match('/\(([^)]+)\)/', $data->groupId, $matches) ?>
          {{ $matches[1] or $data->groupId }}
        </div>
      </div>
    </div>

    <div class="card billing-latest">
      <div>
        <?php $latest = $data->history[0] ?>
        <h1>{{ $latest->periode }}</h1>

        <table width="100%">
          @foreach($latest->detail as $detail)
            <tr>
              <th colspan="3">{{ $detail->layanan }}</th>
            </tr>
            <tr class="text-big">
              <td width="1">Rp</td>
              <td align="right">{{ $detail->tagihan }}</td>
              <td width="1">
                @if ($detail->statusBayar == 'Lunas')
                  <i class="fa fa-check-square-o text-success"></i>
                @else
                  <i class="fa fa-square-o"></i>
                @endif
              </td>
            </tr>
          @endforeach
        </table>
      </div>


      <?php $class = ($latest->statusBayar == 'Lunas') ? 'bg-success' : 'bg-inverse' ?>
      <div class="card-footer {{ $class }}">
        <div class="text-big pull-left">Rp</div>
        <div class="text-big pull-right">
          <span>{{ $latest->tagihan }}</span>
          @if ($latest->statusBayar == 'Lunas')
            <i class="fa fa-check-square-o"></i>
          @else
            <i class="fa fa-square-o"></i>
          @endif
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>

  <div class="history-previous row">
    {{-- Start from index 1; ignores first entry (index 0) --}}
    @for($i = 1; $i < count($data->history); $i++)
      <?php $entry = $data->history[$i] ?>
      <?php $class = ($entry->statusBayar == 'Lunas') ? 'success' : 'inverse' ?>
      <div class="col-xs-4">
        <div class="panel panel-{{ $class }}">
          <div class="panel-heading">{{ $entry->periode }}</div>
          <div class="panel-body text-big">
            {{--<div class="text-big pull-left">Rp</div>--}}
            <div class="text-big text-right">
              <span>{{ $entry->tagihan }}</span>
              @if ($entry->statusBayar == 'Lunas')
                <i class="fa fa-check-square-o text-success"></i>
              @else
                <i class="fa fa-square-o"></i>
              @endif
            </div>
          </div>
        </div>
      </div>
    @endfor
  </div>
@endsection
