<ul class="tree">
  @foreach($list as $item)
    <li>
      @if (isset($getUrl))
        <a href="{{ $getUrl($item) }}">{{ $item->nama }}</a>
      @else
        <span>{{ $item->nama }}</span>
      @endif

      @if (count($item->children))
        @include('partial.workzone', ['list' => $item->children])
      @endif
    </li>
  @endforeach
</ul>
