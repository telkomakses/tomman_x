<div id="{{ $modalId }}" class="modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">
          <span>&times;</span>
        </button>
        <h4 class="modal-title">
          <span>{{ $modalTitle ?: 'Koordinat' }}</span>
          (<span id="latText"></span>, <span id="lngText"></span>)
        </h4>
      </div>
      <div class="modal-body">
        <div id="mapView"></div>
      </div>
      @if ($isWritable)
        <div class="modal-footer">
          <button id="btnGetGps" class="btn btn-default pull-left">GPS</button>
          <button class="btn btn-default" data-dismiss="modal">Batal</button>
          <button id="btnGetMarker" class="btn btn-primary">OK</button>
        </div>
      @endif
    </div>
  </div>
</div>

@include('partial.googlemap')

<script>
  (function() {
    /* jshint ignore:start */
    window.isWritable = {{ $isWritable ? 'true' : 'false' }};
    /* jshint ignore:end */

    document.getElementById('mapView').style.height = (window.innerHeight - 250) + 'px';

    var $mapModal   = $('#{{ $modalId }}');
    var latTextEl   = document.getElementById('latText');
    var lngTextEl   = document.getElementById('lngText');

    var map = null, marker = null, onSelectedCallback = null;

    var showMarker = function(latLng) {
      if (!marker) {
        marker = new google.maps.Marker({
          position: latLng,
          map: map,
          draggable: window.isWritable,
          animation: google.maps.Animation.BOUNCE
        });
        marker.addListener('dragend', mapActionHandler);
      } else {
        marker.setVisible(true);
        marker.setPosition(latLng);
        marker.setAnimation(google.maps.Animation.BOUNCE);
      }
    };

    var mapActionHandler = function(event) {
      latTextEl.textContent = event.latLng.lat();
      lngTextEl.textContent = event.latLng.lng();

      showMarker(event.latLng);
    };

    var initMap = function(lat, lng) {
      var center = {
        lat: lat || 0.9804109,
        lng: lng || 114.0140993
      };
      var withMarker = (!isNaN(lat) && !isNaN(lng)) && (lat !== 0 && lng !== 0);
      var zoom = withMarker ? 17 : 7;

      if (!map) {
        map = new google.maps.Map(
          document.getElementById('mapView'),
          {
            center: center,
            zoom: zoom,
            clickableIcons: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }
        );
        if (window.isWritable) {
          map.addListener('click', mapActionHandler);
        }
      } else {
        map.setZoom(zoom);
        map.panTo(center);
      }

      if (withMarker) {
        showMarker(center);

        latTextEl.textContent = center.lat;
        lngTextEl.textContent = center.lng;
      } else if (marker) {
        marker.setVisible(false);
      }
    };

    $mapModal.on('shown.bs.modal', function() {
      var val = $coordInput.val().split(',');
      if (val.length !== 2) {
        initMap();
      } else {
        initMap(
          Number(val[0].trim()),
          Number(val[1].trim())
        );
      }
    });

    var $btnGetGps = $('#btnGetGps');
    $btnGetGps.click(function() {
      $btnGetGps.prop('disabled', true);
      $btnGetGps.html('<i class="fa fa-spin fa-spinner"></i>');

      navigator.geolocation.getCurrentPosition(
        function(gps) {
          var latlng = {
            lat: gps.coords.latitude,
            lng: gps.coords.longitude
          };
          map.setZoom(17);
          map.panTo(latlng);
          showMarker(latlng);

          latTextEl.textContent = latlng.lat;
          lngTextEl.textContent = latlng.lng;

          $btnGetGps.prop('disabled', false);
          $btnGetGps.html('GPS');
        },
        function(err) {
          if (err.code === 1) alert('ERROR: GPS Tidak Aktif\n\nAktifkan GPS kemudian coba lagi');
          else alert('ERROR: ' + err.code + '\n' + err.message);

          $btnGetGps.prop('disabled', false);
          $btnGetGps.html('GPS');
        },
        {
          enableHighAccuracy: true
        }
      );
    });

    $('#btnGetMarker').click(function() {
      const latLng = marker ? marker.getPosition() : new google.maps.LatLng(0,0);
      const result = {
        lat: latLng.lat(),
        lng: latLng.lng()
      };

      if (onSelectedCallback) onSelectedCallback(result);

      $mapModal.modal('hide');
    });

    window.MapMarkerSelector = {
      open: function(lat, lng, callback) {
        initMap(Number(lat), Number(lng));

        onSelectedCallback = callback;
        $mapModal.modal();
      }
    };
  })();
</script>
