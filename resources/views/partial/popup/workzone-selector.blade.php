<div id="{{ $modalId }}" class="modal" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal" type="button">
          <span>&times;</span>
        </button>
        <h4 class="modal-title">Select Work Zone</h4>
      </div>
      <div class="modal-body">
        <workzone-tree :items="workzoneList"></workzone-tree>
      </div>
      <div class="modal-footer">
        <button class="btn btn-default" data-dismiss="modal" type="button">Batal</button>
      </div>
    </div>
  </div>
</div>

<script id="template-tree" type="text/x-template">
  <ul class="tree">
    <li v-for="(item, index) in items" :key="item.id">
      <div class="workzone-item tree-selectable" v-on:click="selectItem(item)">@{{ item.nama }}</div>

      <workzone-tree :items="item.children" :parent="item"></workzone-tree>
    </li>
  </ul>
</script>

<script>
  (function() {
    /* jshint ignore:start */
    const workzoneData = {!! json_encode($workzoneData) !!};
    const $modal = $('#{{ $modalId }}');
    /* jshint ignore:end */

    const deserializePath = function(path) {
      let result = [];
      let currentArray = vm.workzoneList;
      const paths = path.split('.');
      paths.forEach(id => {
        const currentItem = currentArray.find(x => x.id == id);
        if (!currentItem) {
          // TODO: error message - item has invalid path
          return;
        }
        result.push(currentItem);
        currentArray = currentItem.children;
      });

      return result;
    };

    let onSelectedCallback = null;
    window.WorkzoneSelector = {
      deserializePath: deserializePath,
      open: function(callback) {
        // TODO: currently selected item
        onSelectedCallback = callback;

        $modal.modal();
      }
    };


    Vue.component('workzone-tree', {
      template: '#template-tree',
      props: {
        parent: {
          'type': Object,
          'default': () => ({})
        },
        items: {
          'type': Array,
          'default': () => []
        }
      },
      methods: {
        selectItem(item) {
          item.pathArray = deserializePath(item.path);

          if (onSelectedCallback) onSelectedCallback(item);

          $modal.modal('hide');
        }
      }
    });

    let vm = new Vue({
      el: $modal.find('.modal-body')[0],
      data: {
        workzoneList: workzoneData
      }
    });

  })();
</script>
