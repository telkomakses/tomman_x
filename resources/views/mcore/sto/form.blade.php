@extends('app')

@section('style')
  <style>
    .form-group .breadcrumb {
      margin-bottom: 0;
    }
  </style>
@endsection

@section('body')
  <div class="row bg-title">
    <div class="col-sm-6"></div>
    <div class="col-sm-6">
      <ol class="breadcrumb">
        <li><a href="/mcore">MCore</a></li>
        <li><a href="/mcore/sto">STO</a></li>
        @if (isset($stoData))
          <li>
            <a href="/mcore/sto/zone/{{ $stoData->workzone_id }}">
              <span class="label label-info">zone</span>
              <span>{{ $stoData->workzone_nama }}</span>
            </a>
          </li>
          <li class="active">{{ $stoData->nama }}</li>
        @else
          <li class="active">
            {{ isset($data) ? $data->nama : 'Input' }}
          </li>
        @endif
      </ol>
    </div>
  </div>

  @include('partial.alerts')

  <form method="post">
    {{ csrf_field() }}
    @if (isset($stoData))
      <input type="hidden" name="id" value="{{ $stoData->id }}">
    @endif
    <div class="white-box">
      <div class="form-group">
        <label for="">Work Zone</label>
        <div id="workzone-view">
          <input name="workzone_id" type="hidden" v-model="workzone.id">
          <p class="form-control-static visible-xs-block" v-if="workzone.id">@{{ workzone.nama }}</p>
          <ol class="breadcrumb hidden-xs" v-if="workzone.id">
            <li v-for="path in workzone.pathArray" :key="path.id">@{{ path.nama }}</li>
          </ol>
        </div>
        <button id="btnBrowseWorkzone" type="button" class="btn btn-default">Select</button>
      </div>

      <div class="form-group">
        <label for="input-nama">Nama STO</label>
        <input name="nama" id="input-nama" type="text" class="form-control" value="{{ old('nama') ?: @$stoData->nama }}">
      </div>

      <div class="form-group">
        <label for="input-koordinat">Koordinat (Lat, Lon)</label>
        <input name="koordinat" id="input-koordinat" type="text" class="form-control" value="{{ old('koordinat') ?: (isset($stoData) ? "{$stoData->lat}, {$stoData->lon}" : '') }}">
        <button id="btnBrowseMap" type="button" class="btn btn-default">Map</button>
      </div>
    </div>
    <button class="btn btn-primary pull-right">
      <i class="fa fa-save"></i>
      <span>Simpan</span>
    </button>
  </form>
@endsection

@section('script')
  @include('partial.popup.workzone-selector', [
    'modalId' => 'workzone-selector',
    'workzoneData' => $workzoneData
  ])

  @include('partial.popup.map-marker', [
    'modalId' => 'map-marker',
    'modalTitle' => 'Koordinat STO',
    'isWritable' => true
  ])

  <script>
    /* jshint ignore:start */
    var selectedWorkzone = {!! json_encode($selectedWorkzone) !!};
    /* jshint ignore:end */

    var workzoneVm = new Vue({
      el: '#workzone-view',
      data: {
        workzone: {
          id: null
        }
      }
    });

    if (selectedWorkzone) {
      let paths = WorkzoneSelector.deserializePath(selectedWorkzone.path);
      let workzone = paths[paths.length - 1];
      workzone.pathArray = paths;

      workzoneVm.workzone = workzone;
    }

    $('#btnBrowseWorkzone').click(function() {
      WorkzoneSelector.open(selected => {
        workzoneVm.workzone = selected;
      });
    });

    const $coordInput = $('#input-koordinat');
    $('#btnBrowseMap').click(function() {
      const vals = $coordInput.val().split(',');
      let lat = null, lng = null;

      if (vals.length === 2) {
        lat = vals[0].trim();
        lng = vals[1].trim();
      } else {
        lat = lng = 0;
      }

      MapMarkerSelector.open(lat, lng, coordinate => {
        $coordInput.val(coordinate.lat + ', ' + coordinate.lng);
      });
    });
  </script>
@endsection
