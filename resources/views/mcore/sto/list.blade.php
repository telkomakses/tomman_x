@extends('app')

@section('style')
  <style>
    .ribbon-wrapper {
      background: white;
    }
    .ribbon-content {
      margin-left: -30px;
    }
    .ribbon-content ul {
      margin-left: -20px;
    }

    @media (min-width: 768px) {
      .ribbon-content ul {
        display: block;
      }
      .ribbon-content ul > li, .ribbon-content ul > li > a {
        display: inline-block;
      }
      /* TODO: colors should adhere to color theme */
      .ribbon-content ul > li > a {
        width: 200px;
        padding: 8px 12px 6px;
        border: 1px solid #0b67cd;
        margin-right: 10px;
      }
      .ribbon-content ul > li > a:hover {
        background: #0b67cd;
        color: white;
      }
    }
  </style>
@endsection

@section('body')
  <div class="row bg-title">
    <div class="col-sm-6">
      <h4 class="page-title hidden-xs">Sentral Telepon Otomat</h4>
      <h4 class="page-title visible-xs-block">STO</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb">
        <li><a href="/mcore">MCore</a></li>
        <li><a href="/mcore/sto">STO</a></li>
        <li class="active">
          <span class="label label-info">zone</span>
          <span>{{ $workzoneData->nama }}</span>
        </li>
      </ol>
    </div>
  </div>

  @foreach($groupedList as $group)
    <div class="ribbon-wrapper m-b-20">
      <div class="ribbon ribbon-default">{{ $group->workzone->nama }}</div>
      <div class="ribbon-content">
        <ul>
          @foreach($group->stoList as $sto)
            <li><a href="/mcore/sto/{{ $sto->id }}">{{ $sto->nama }}</a></li>
          @endforeach
        </ul>
      </div>
    </div>
  @endforeach
@endsection
