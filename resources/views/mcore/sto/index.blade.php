@extends('app')

@section('title', 'STO - TOMMAN')

@section('body')
  <div class="row bg-title">
    <div class="col-sm-6">
      <h4 class="page-title hidden-xs">Sentral Telepon Otomat</h4>
      <h4 class="page-title visible-xs-block">STO</h4>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb">
        <li><a href="/mcore">MCore</a></li>
        <li class="active">STO</li>
      </ol>
    </div>
  </div>

  <a href="/mcore/sto/create" class="btn btn-default">input</a>

  <div class="white-box">
    <h4>Pilih Work Zone</h4>
    <div class="table-responsive">
      <?php $getUrl = function ($item) { return "/mcore/sto/zone/{$item->id}"; } ?>
      @include('partial.workzone', ['list' => $workzoneList, 'getUrl' => $getUrl])
    </div>
  </div>
@endsection
