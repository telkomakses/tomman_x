@extends('app')

@section('style')
  <style>
    .container-fluid {
      padding: 0;
    }
  </style>
@endsection

@section('body')
  <div id="mapView"></div>
@endsection

@section('script')
  <script src="/js/mcore-map.js"></script>
  @include('partial.googlemap', ['initMapCallback' => 'onMapApiReady'])
@endsection
