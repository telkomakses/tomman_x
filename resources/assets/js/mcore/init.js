var center = {
  lat: -0.26367,
  lng: 118.21289
};
var zoom = 5;

/**
 * Coordinates:
 *  Indonesia
 *    ZOOM: 5
 *    NE: 15.623037740985408 141.13037046875002
 *    SW: -16.130261104864633 95.29540953125002
 *
 *  KALSEL
 *    ZOOM: 10
 *    NE: -3.26117621536941 115.75057920898439
 *    SW: -4.2642453925806665 114.31823667968752
 *
 *  Banjarmasin
 *    ZOOM: 14
 *    NE: -3.2891113183541045 114.63898596252443
 *    SW: -3.3518339024335413 114.54946455444338
 *
 *  Balikpapan
 *    ZOOM: 14
 *    NE: -1.216643454191983 116.89521727050783
 *    SW: -1.2794566102397533 116.80569586242677
 *
 *  ALPRO
 *    ZOOM: 17
 *    NE: -1.2456260768965182 116.86645326103212
 *    SW: -1.253477717303405 116.85526308502199
 */

// TODO: mapstate cookie
window.onMapApiReady = function() {
  const $mapView = $('#mapView');
  $mapView.css('height', window.innerHeight - 68);

  window.map = new google.maps.Map(
    $mapView[0],
    {
      center: center,
      zoom: zoom
    }
  );

  window.getBounds = function() {
    "use strict";
    var b = map.getBounds();
    var ne = b.getNorthEast();
    var sw = b.getSouthWest();
    console.log('NE:', ne.lat(), ne.lng());
    console.log('SW:', sw.lat(), sw.lng());
  }

};
