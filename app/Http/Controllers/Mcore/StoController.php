<?php namespace App\Http\Controllers\Mcore;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\CommonData\WorkzoneCached;
use App\Service\CommonData\Workzone;
use App\Service\Mcore\Sto;
use App\Service\SessionHelper;

class StoController extends Controller
{
    public function index()
    {
        // TODO: not modified response
        // TODO: check user's workzone
        list($workzoneList, $_) = WorkzoneCached::all();
        return view('mcore.sto.index', compact('workzoneList'));
    }

    public function inputForm(Request $request)
    {
        // TODO: not modified response

        // TODO: check user privilege
        //$currentUser = SessionHelper::getCurrentUser();

        $selectedWorkzone = null;
        $selectedWorkzoneId = old('workzone_id');
        if ($selectedWorkzoneId) {
            list($selectedWorkzone, $_) = WorkzoneCached::getById($selectedWorkzoneId);
        }

        list($workzoneData, $_) = WorkzoneCached::all();

        return view('mcore.sto.form', compact('workzoneData', 'selectedWorkzone'));
    }

    public function create(Request $request)
    {
        // TODO: VALIDATION: workzone & nama is required
        $in = (object)$request->all();
        $gps = explode(',', $in->koordinat);
        if (count($gps) == 2) {
            $lat = trim($gps[0]);
            $lon = trim($gps[1]);
        } else {
            $lat = $lon = 0;
        }

        try {
            $id = Sto::create(
                $in->workzone_id,
                $in->nama,
                $lat,
                $lon
            );
        } catch (QueryException $e) {
            return back()
                ->withInput()
                ->with('alerts', [
                    [
                        'type' => 'danger',
                        'text' => '<strong>DB ERROR</strong><br>'.$e->getMessage()
                    ]
                ])
            ;
        }

        return redirect("mcore/sto/{$id}")->with('alerts', [
            [
                'type' => 'success',
                'text' => '<strong>Sukses</strong> menambah data'
            ]
        ]);
    }

    public function show($id)
    {
        // TODO: determine editability
        // TODO: cache and last modified

        $stoData = Sto::getById($id);
        $selectedWorkzone = (object)[
            'id' => $stoData->workzone_id,
            'nama' => $stoData->workzone_nama,
            'path' => $stoData->workzone_path
        ];
        list($workzoneData, $_) = WorkzoneCached::all();

        return view('mcore.sto.form', compact('workzoneData', 'stoData', 'selectedWorkzone'));
    }

    public function update(Request $request)
    {
        $in = (object)$request->all();
        $gps = explode(',', $in->koordinat);
        if (count($gps) == 2) {
            $lat = trim($gps[0]);
            $lon = trim($gps[1]);
        } else {
            $lat = $lon = 0;
        }

        try {
            Sto::update(
                $in->id,
                $in->workzone_id,
                $in->nama,
                $lat,
                $lon
            );
        } catch (QueryException $e) {
            return back()
                ->withInput()
                ->with('alerts', [
                    [
                        'type' => 'danger',
                        'text' => '<strong>DB ERROR</strong><br>'.$e->getMessage()
                    ]
                ])
            ;
        }

        return back()
            ->with('alerts', [
                [
                    'type' => 'success',
                    'text' => '<strong>Berhasil</strong> meng<em>update</em> data'
                ]
            ])
        ;
    }

    public function listByWorkzone($workzoneId)
    {
        // TODO: cache & not modified response
        $workzoneData = Workzone::getById($workzoneId);
        $workzoneList = Workzone::getFlatTrunkByPath($workzoneData->path);

        $mapper = function ($workzone) {
            return $workzone->id;
        };
        $workzoneIdList = array_map($mapper, $workzoneList->all());

        $stoList = Sto::findByWorkzoneIdList($workzoneIdList);

        $groupedList = [];
        $currentGroup = null;
        $lastWz = null;
        foreach ($stoList as $sto) {
            if ($lastWz != $sto->workzone_id) {
                $lastWz = $sto->workzone_id;
                $currentGroup = (object)[
                    'workzone' => (object)[
                        'id' => $sto->workzone_id,
                        'nama' => $sto->workzone_nama,
                    ],
                    'stoList' => []
                ];
                $groupedList[] = $currentGroup;
            }
            $currentGroup->stoList[] = $sto;
        }

        return view('mcore.sto.list', compact('workzoneData', 'workzoneList', 'stoList', 'groupedList'));
    }
}
