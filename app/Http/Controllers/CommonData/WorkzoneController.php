<?php namespace App\Http\Controllers\CommonData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service\HttpHelper;
use App\Service\CommonData\WorkzoneCached;

class WorkzoneController extends Controller
{
    public function index(Request $request)
    {
        $path = resource_path('views/workzone/index.blade.php');
        list($requireContent, $lastModified) = HttpHelper::lastModifiedByRequestAndFile($request, $path);

        if ($requireContent) {
            return response()
                ->view('workzone.index')
                ->header('Last-Modified', $lastModified);
        }

        return response('NOT MODIFIED', 304)
            ->header('Last-Modified', $lastModified);
    }

    public function apiAll()
    {
        // TODO: 304 NOT MODIFIED
        list($all, $lastModified) = WorkzoneCached::all();

        return response()->json($all);
    }

    public function apiCreate(Request $request)
    {
        $nama = $request->input('nama');
        $path = $request->input('path', null);

        $result = WorkzoneCached::create($nama, $path);

        return response()->json($result);
    }

    public function apiUpdate(Request $request)
    {
        $id   = $request->input('id');
        $nama = $request->input('nama');

        $result = WorkzoneCached::update($id, $nama);

        return response()->json($result);
    }
}
