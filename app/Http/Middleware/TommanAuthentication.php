<?php

namespace App\Http\Middleware;

use App\Service\SessionHelper;
use App\Service\Authentication\User;
use Closure;

class TommanAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (SessionHelper::hasCurrentUser()) {
            return $next($request);
        }

        $rememberToken = $request->cookie('persistent-token');
        if ($rememberToken) {
            $user = User::getByRememberToken($rememberToken);
            if ($user) {
                SessionHelper::setCurrentUser($user);
                return $next($request);
            }
        }

        SessionHelper::setLoginRedirect($request->fullUrl());
        // TODO: test fetch/ServiceWorker
        if ($request->ajax()) {
            return response('UNAUTHORIZED', 401);
        } else {
            return redirect('login');
        }
    }
}
