"use strict";
// modified from GIST:
// https://gist.github.com/schnerd/b5f9b248d84ce147d43ca292de30ada1

const chromeLauncher = require('chrome-launcher');
const CDP = require('chrome-remote-interface');
const argv = require('minimist')(process.argv.slice(2));
const file = require('fs');

// CLI Args
const url = argv.url;
const format = argv.format === 'jpeg' ? 'jpeg' : 'png';
const outputPath = argv.output || 'screenshot.png';
const viewportWidth = argv.width || 720;
const viewportHeight = argv.height || 1280;
const delay = argv.delay || 500;
const useMobileView = argv.mobile || false;

const fullPage = true;
const EXITCODE_ERR = 1;
const EXITCODE_SCREENSHOT_FAIL = 2;

let exitCode = 0;

(async () => {
  const chrome = await chromeLauncher.launch({
    chromeFlags: [
      '--disable-web-security',
      '--disable-gpu',
      '--hide-scrollbars',
      '--headless'
    ]
  });

  // Start the Chrome Debugging Protocol
  try {
    const client = await CDP({port: chrome.port});
    // Extract used DevTools domains.
    const {
      DOM, Emulation, Network, Page, Runtime
    } = client;

    // Set up viewport resolution, etc.
    const deviceMetrics = {
      width: viewportWidth,
      height: viewportHeight,
      deviceScaleFactor: 0,
      mobile: useMobileView,
      fitWindow: false,
    };
    await Emulation.setDeviceMetricsOverride(deviceMetrics);
    await Emulation.setVisibleSize({
      width: viewportWidth,
      height: viewportHeight
    });

    // Enable events on domains we are interested in.
    await Page.enable();
    await DOM.enable();
    await Network.enable();

    // Navigate to target page
    await Page.navigate({
      url
    });

    // Wait for page load event to take screenshot
    await Page.loadEventFired();

    // If the `full` CLI option was passed, we need to measure the height of
    // the rendered page and use Emulation.setVisibleSize
    if (fullPage) {
      const {
        result: {
          value: height
        }
      } = await Runtime.evaluate({
        expression:
          `Math.max(
            document.body.scrollHeight, 
            document.body.offsetHeight, 
            document.documentElement.clientHeight, 
            document.documentElement.scrollHeight, 
            document.documentElement.offsetHeight
          )`
      });
      await Emulation.setVisibleSize({
        width: viewportWidth,
        height: height
      });
      // This forceViewport call ensures that content outside the viewport is
      // rendered, otherwise it shows up as grey. Possibly a bug?
      await Emulation.forceViewport({
        x: 0,
        y: 0,
        scale: 1
      });
    }

    if (delay) {
      await new Promise(resolve => setTimeout(resolve, delay));
    }

    const screenshot = await Page.captureScreenshot({
      format
    });
    const buffer = new Buffer(screenshot.data, 'base64');

    await new Promise((resolve, reject) => {
      file.writeFile(outputPath, buffer, 'base64', function (err) {
        client.close();

        if (err) {
          console.error(err);
          exitCode = EXITCODE_SCREENSHOT_FAIL;
          reject();
          return;
        }

        resolve();
      });
    });
  } catch (err) {
    console.error(err);
    exitCode = EXITCODE_ERR;
  } finally {
    await chrome.kill();
    process.exit(exitCode);
  }
})();
