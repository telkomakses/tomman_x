"use strict";

const HeadlessChrome = require('simple-headless-chrome');
const QS = require('querystring');

const URL = 'http://i-payment.telkom.co.id/script/intag_search_trems.php';
// const URL = 'http://10.60.165.60/script/intag_search_trems.php';
const REQUESTER = {
  name: 'Hadi Susilo',
  addr: 'Banjarmasin',
  phone: '081253680725'
};

const EXITCODE_INVALID_PARAM = 1;
const EXITCODE_BROWSER_FAILURE = 2;

// input validator using regex
const numberOnly = /^[0-9]+$/;

/**
 * command line parameter
 * contoh:
 * node ipayment.js <jastel>
 * ./ipayment.js <jastel>
 */
var jastel = process.argv[2];
if (!numberOnly.test(jastel)) {
  console.log(JSON.stringify({
    error: 'InvalidArgument'
  }));
  process.exit(EXITCODE_INVALID_PARAM);
}

const browser = new HeadlessChrome();
(async() => {
  try {
    const param = QS.stringify({
      phone: jastel,
      via: ['DWH', 'TREMS'],

      rname: REQUESTER.name,
      raddr: REQUESTER.addr,
      rphone: REQUESTER.phone
    });
    const requestUrl = `${URL}?${param}`;

    await browser.init();
    await browser.goTo(requestUrl);

    const {
      result: {
        value: data
      }
    } = await browser.evaluate(() => {
      var result;
      var infoEl = document.querySelector('body > table:nth-child(2) > tbody');

      function getTextContent(selector, el = document) {
        var s = '';
        var e = el.querySelector(selector);
        if (e) s = e.textContent;
        return s || 'N/A';
      }

      result = {
        nama: getTextContent('tr:nth-child(1) > td.value', infoEl),
        produk: getTextContent('tr:nth-child(2) > td.value', infoEl),
        phone: getTextContent('tr:nth-child(3) > td.value', infoEl),
        internet: getTextContent('tr:nth-child(4) > td.value', infoEl),
        groupId: getTextContent('tr:nth-child(5) > td.value', infoEl)
      };

      var history = [];
      var historyEl = document.querySelectorAll('body > table:nth-child(4) > tbody > tr');
      historyEl.forEach((tr, i) => {
        //skip first entry (th row)
        if (i === 0) return;

        //baris genap: history entry
        if (i % 2) {
          var h = {
            periode: getTextContent('td:nth-child(2)', tr),
            mataUang: getTextContent('td:nth-child(3)', tr),
            tagihan: getTextContent('td:nth-child(4)', tr),
            belumBayar: getTextContent('td:nth-child(5)', tr),
            statusBayar: getTextContent('td:nth-child(6)', tr),
            lokasiBayar: getTextContent('td:nth-child(7)', tr),
            cicilan: getTextContent('td:nth-child(8)', tr),
            tanggal: getTextContent('td:nth-child(9)', tr),
            jam: getTextContent('td:nth-child(10)', tr)
          };
          history.push(h);
        }
        //baris ganjil: history detail
        else {
          var d = [];

          var detailEl = tr.querySelectorAll('table tr');
          detailEl.forEach(function(row, i) {
            // skip first entry (th row)
            if (i === 0) return;

            d.push({
              layanan: getTextContent('td:nth-child(1)', row),
              mataUang: getTextContent('td:nth-child(2)', row),
              tagihan: getTextContent('td:nth-child(3)', row),
              statusBayar: getTextContent('td:nth-child(4)', row),
              lokasiBayar: getTextContent('td:nth-child(5)', row)
            });
          });

          history[history.length - 1].detail = d;
        }
      });

      result.history = history;

      return result;
    });

    // primary output
    console.log(JSON.stringify(data));
  }
  catch (err) {
    console.log(JSON.stringify({
      error: err
    }));
    process.exit(EXITCODE_BROWSER_FAILURE);
  }
  finally {
    await browser.close(true);
    process.exit();
  }
})();
