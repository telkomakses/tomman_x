<?php namespace App\Service;

class HttpHelper
{
    public static function lastModified($ifModifiedSince, int $lastModified)
    {
        $result = [true, ''];

        $reqModified = strtotime($ifModifiedSince);
        if ($reqModified && $reqModified >= $lastModified) {
            $result[0] = false;
        }

        $result[1] = gmdate('D, d M Y H:i:s T', $lastModified);
        return $result;
    }

    public static function lastModifiedByRequestAndFile($request, string $filePath)
    {
        $reqModified = $request->header('If-Modified-Since');
        $resModified = filemtime($filePath);

        return self::lastModified($reqModified, $resModified);
    }
}
