<?php

namespace App\Service;

class WebScreenShot
{
    /**
     * Rasterize existing HTML file and write the output into file
     *
     * @param string $srcPath path to input html file
     * @param string $dstPath output path
     * @param int width of viewport. height is calculated automatically
     */
    public static function createFromFile($srcPath, $dstPath, $width = 720)
    {
        $exepath  = escapeshellarg(__DIR__.'/WebScreenShot.js');
        $url      = escapeshellarg("file:///$srcPath");
        $dstPath  = escapeshellarg($dstPath);
        $width    = escapeshellarg($width);

        $cmd      = "node $exepath --url $url --output $dstPath --width $width";

        exec($cmd, $output, $exitCode);
        // TODO: check $exitCode for nonzero error
    }
}
