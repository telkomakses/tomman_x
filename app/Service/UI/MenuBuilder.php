<?php namespace App\Service\UI;

class MenuBuilder
{
    public static function build($permissions)
    {
        //TODO
        return [
            self::commonDataMenu(),
            self::mcoreMenu(),
            (object)[
                'label' => 'Shell Test',
                'url' => '/shell-test'
            ]
        ];
    }

    protected static function commonDataMenu()
    {
        //TODO
        return (object)[
            'label' => 'Data Umum',
            'children' => [
                (object)[
                    'label' => 'Work Zone',
                    'url' => '/data/workzone'
                ]
            ]
        ];
    }

    protected static function mcoreMenu()
    {
        //TODO
        return (object)[
            'label' => 'MCore',
            'children' => [
                (object)[
                    'label' => 'Map',
                    'url' => '/mcore'
                ],
                (object)[
                    'label' => 'STO',
                    'url' => '/mcore/sto'
                ]
            ]
        ];
    }
}
