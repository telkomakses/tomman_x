<?php namespace App\Service\CommonData;

use Illuminate\Support\Facades\Cache;

class WorkzoneCached
{
    private static function cache()
    {
        // TODO: separate list and individual cache
        return Cache::tags('CommonData.Workzone');
    }

    public static function all()
    {
        $result = self::cache()->rememberForever('all', function () {
            return Workzone::all();
        });
        $lastModified = self::lastModifiedAll();

        return [$result, $lastModified];
    }

    public static function getById($id)
    {
        $result = self::cache()->rememberForever('id-'.$id, function () use ($id) {
            return Workzone::getById($id);
        });
        $lastModified = self::lastModifiedAll();

        return [$result, $lastModified];
    }

    public static function lastModifiedAll()
    {
        $lastModified = self::cache()->rememberForever('all:ts', function () {
            return time();
        });

        return $lastModified;
    }

    public static function create(string $nama, string $path = null)
    {
        self::cache()->flush();

        return Workzone::create($nama, $path);
    }

    public static function update($id, $nama)
    {
        self::cache()->flush();

        return Workzone::update($id, $nama);
    }
}
