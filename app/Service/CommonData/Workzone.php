<?php namespace App\Service\CommonData;

use Illuminate\Support\Facades\DB;

class Workzone
{
    private static function db()
    {
        return DB::table('workzone');
    }

    public static function all()
    {
        $all = self::db()->orderBy('path')->get();
        $result = self::grow($all);

        return $result;
    }

    public static function getById($id)
    {
        return self::db()->where('id', $id)->first();
    }

    public static function getFlatTrunkById($id)
    {
        $data = self::getById($id);
        return self::getFlatTrunkByPath($data->path);
    }

    public static function getFlatTrunkByPath($path)
    {
        return self::db()->where('path', '<@', $path)->get();
    }

    public static function create(string $nama, string $path = null)
    {
        $id = null;
        DB::transaction(function () use (&$id, $nama, $path) {
            $id = self::db()->insertGetId([
                'nama' => $nama
            ]);

            $path = !empty($path) ? "$path.$id" : $id;

            self::db()
                ->where('id', $id)
                ->update(['path' => $path]);
        });

        $row = self::db()
            ->where('id', $id)
            ->first();

        return $row;
    }

    public static function update($id, $nama)
    {
        self::db()->where('id', $id)->update(['nama' => $nama]);

        $row = self::db()
            ->where('id', $id)
            ->first();

        return $row;
    }

    public static function grow($rows)
    {
        $result = [];
        $previousRows = [];

        foreach ($rows as $row) {
            $row->children = [];

            while (count($previousRows)) {
                $previous = array_pop($previousRows);
                $path = $previous->path .'.'.$row->id;
                if ($path == $row->path) {
                    $previous->children[] = $row;
                    $previousRows[] = $previous;
                    $previousRows[] = $row;

                    continue 2;
                } else {
                    usort($previous->children, [__CLASS__, 'sortHandler']);
                }
            }

            $previousRows[] = $row;
            $result[] = $row;
        }

        usort($result, [__CLASS__, 'sortHandler']);
        return $result;
    }

    private static function sortHandler($a, $b)
    {
        return strnatcmp($a->nama, $b->nama);
    }
}
