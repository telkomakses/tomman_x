<?php namespace App\Service\Mcore;

use Illuminate\Support\Facades\DB;
use App\Service\CommonData\Workzone;

class Sto
{
    private static function db()
    {
        return DB::table('mcore.sto');
    }

    private static function join()
    {
        return self::db()
            ->leftJoin('workzone', 'workzone.id', '=', 'workzone_id')
            ->select(
                'sto.*',
                'workzone.nama AS workzone_nama',
                'workzone.path AS workzone_path',
                DB::raw('ST_X(koordinat) AS lon'),
                DB::raw('ST_Y(koordinat) AS lat')
            )
        ;
    }

    public static function create($workzoneId, $nama, $lat, $lon)
    {
        $pdo = DB::connection()->getPdo();
        $lat = $pdo->quote($lat);
        $lon = $pdo->quote($lon);

        $id = self::db()->insertGetId([
            'workzone_id' => $workzoneId,
            'nama' => $nama,
            'koordinat' => DB::raw("ST_SetSRID(ST_MakePoint({$lon}, {$lat}), 4326)")
        ]);

        return $id;
    }

    public static function getById($id)
    {
        return self::join()
            ->where('sto.id', $id)
            ->first()
        ;
    }

    public static function update($id, $workzoneId, $nama, $lat, $lon)
    {
        $pdo = DB::connection()->getPdo();
        $lat = $pdo->quote($lat);
        $lon = $pdo->quote($lon);

        return self::db()
            ->where('id', $id)
            ->update([
                'workzone_id' => $workzoneId,
                'nama' => $nama,
                'koordinat' => DB::raw("ST_SetSRID(ST_MakePoint({$lon}, {$lat}), 4326)")
            ])
        ;
    }

    public static function findByWorkzoneIdList($workzoneIdList)
    {
        return self::join()
            ->whereIn('workzone_id', $workzoneIdList)
            ->orderBy('workzone_nama')
            ->get();
    }
}
