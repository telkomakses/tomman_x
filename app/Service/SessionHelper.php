<?php namespace App\Service;

use Illuminate\Support\Facades\Session;

class SessionHelper
{
    const KEY_CURRENT_USER = 'auth';
    const KEY_LOGIN_REDIRECT = 'auth-originalUrl';

    public static function hasCurrentUser()
    {
        return Session::has(self::KEY_CURRENT_USER);
    }

    public static function getCurrentUser()
    {
        return Session::get(self::KEY_CURRENT_USER);
    }

    public static function setCurrentUser($user)
    {
        Session::put(self::KEY_CURRENT_USER, $user);
    }

    public static function hasLoginRedirect()
    {
        return Session::has(self::KEY_LOGIN_REDIRECT);
    }

    public static function pullLoginRedirect()
    {
        return Session::pull(self::KEY_LOGIN_REDIRECT);
    }

    public static function setLoginRedirect($url)
    {
        Session::put(self::KEY_LOGIN_REDIRECT, $url);
    }

    public static function flush()
    {
        Session::flush();
    }
}
