<?php

namespace App\Providers;

use App\Service\SessionHelper;
use App\Service\UI\MenuBuilder;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // set custom storage directory
        $path = env('PATH_STORAGE', false);
        if ($path) {
            if (!file_exists($path)) {
                // make sure the path have necessary structure
                $origin = realpath(__DIR__ . '/../..').DIRECTORY_SEPARATOR.'storage';
                \File::copyDirectory($origin, $path);
            }

            $this->app->useStoragePath($path);
        }

        View::composer('app.layout', function ($view) {
            $auth = SessionHelper::getCurrentUser();

            // Authenticated User
            $view->with('sessionUser', $auth);

            // Main Menu
            $mainMenu = MenuBuilder::build($auth->permission);
            $view->with('mainMenu', $mainMenu);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
