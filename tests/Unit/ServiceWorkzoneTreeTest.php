<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Service\CommonData\Workzone;

class ServiceWorkzoneTreeTest extends TestCase
{
    public function test_grow_tree()
    {
        $input = [
            (object)[
                'id' => 1,
                'nama' => '1',
                'path' => '1'
            ],
            (object)[
                'id' => 4,
                'nama' => '1-1',
                'path' => '1.4'
            ],
            (object)[
                'id' => 7,
                'nama' => '1-1-1',
                'path' => '1.4.7'
            ],
            (object)[
                'id' => 5,
                'nama' => '1-2',
                'path' => '1.5'
            ],
            (object)[
                'id' => 2,
                'nama' => '2',
                'path' => '2'
            ],
            (object)[
                'id' => 6,
                'nama' => '2-1',
                'path' => '2.6'
            ],
            (object)[
                'id' => 3,
                'nama' => '3',
                'path' => '3'
            ]
        ];

        $expectedResult = [
            (object)[
                'id' => 1,
                'nama' => '1',
                'path' => '1',

                'children' => [
                    (object)[
                        'id' => 4,
                        'nama' => '1-1',
                        'path' => '1.4',

                        'children' => [
                            (object)[
                                'id' => 7,
                                'nama' => '1-1-1',
                                'path' => '1.4.7',

                                'children' => []
                            ]
                        ]
                    ],
                    (object)[
                        'id' => 5,
                        'nama' => '1-2',
                        'path' => '1.5',

                        'children' => []
                    ]
                ]
            ],
            (object)[
                'id' => 2,
                'nama' => '2',
                'path' => '2',

                'children' => [
                    (object)[
                        'id' => 6,
                        'nama' => '2-1',
                        'path' => '2.6',

                        'children' => []
                    ]
                ]
            ],
            (object)[
                'id' => 3,
                'nama' => '3',
                'path' => '3',

                'children' => []
            ]
        ];

        $result = Workzone::grow($input);
        $this->assertEquals($expectedResult, $result);
    }

    public function test_grow_sorted()
    {
        $input = [
            (object)[
                'id' => 1,
                'nama' => 'B',
                'path' => '1'
            ],
            (object)[
                'id' => 4,
                'nama' => 'B-B',
                'path' => '1.4'
            ],
            (object)[
                'id' => 7,
                'nama' => 'B-B-A',
                'path' => '1.4.7'
            ],
            (object)[
                'id' => 5,
                'nama' => 'B-A',
                'path' => '1.5'
            ],
            (object)[
                'id' => 2,
                'nama' => 'D',
                'path' => '2'
            ],
            (object)[
                'id' => 6,
                'nama' => 'D-1',
                'path' => '2.6'
            ],
            (object)[
                'id' => 3,
                'nama' => 'A',
                'path' => '3'
            ]
        ];

        $expectedResult = [
            (object)[
                'id' => 3,
                'nama' => 'A',
                'path' => '3',

                'children' => []
            ],
            (object)[
                'id' => 1,
                'nama' => 'B',
                'path' => '1',

                'children' => [
                    (object)[
                        'id' => 5,
                        'nama' => 'B-A',
                        'path' => '1.5',

                        'children' => []
                    ],
                    (object)[
                        'id' => 4,
                        'nama' => 'B-B',
                        'path' => '1.4',

                        'children' => [
                            (object)[
                                'id' => 7,
                                'nama' => 'B-B-A',
                                'path' => '1.4.7',

                                'children' => []
                            ]
                        ]
                    ]
                ]
            ],
            (object)[
                'id' => 2,
                'nama' => 'D',
                'path' => '2',

                'children' => [
                    (object)[
                        'id' => 6,
                        'nama' => 'D-1',
                        'path' => '2.6',

                        'children' => []
                    ]
                ]
            ]
        ];

        $result = Workzone::grow($input);
        $this->assertEquals($expectedResult, $result);
    }
}
